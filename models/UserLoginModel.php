<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;

    class UserLoginModel extends Model {
        protected function getFieldList(): array {
            return [
                'user_login_id' => Field::readonlyInteger(20),
                'created_at'    => Field::readonlyDateTime(),

                'user_id'       => Field::editableInteger(10),
                'ip_address'    => Field::editableString(24),
                'user_agent'    => Field::editableString(255)
            ];
        }

        public function getAllByUserId(int $userId): array {
            $sql = 'SELECT * FROM user_login WHERE user_id = ?;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute( [ $userId ] );

            $items = [];
            if ( $res ) {
                $items = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $items;
        }
    }
