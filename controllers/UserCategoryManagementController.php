<?php
    namespace App\Controllers;

    class UserCategoryManagementController extends \App\Core\Role\UserRoleController {
        public function categories() {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
        }

        public function getEdit($categoryId) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($categoryId);

            if (!$category) {
                $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('category', $category);
        }

        public function postEdit($categoryId) {
            $newName = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            
            $categoryModel->editById($categoryId, [
                'name' => $newName
            ]);

            $this->redirect(\Configuration::BASE . 'user/categories');
        }

        public function getAdd() {
            
        }

        public function postAdd() {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            
            $categoryId = $categoryModel->add([
                'name' => $name
            ]);

            if (!$categoryId) {
                $this->set('message', 'Nije uspesno izvrseno dodavanje nove kategorije.');
                return;
            }

            $this->redirect(\Configuration::BASE . 'user/categories');
        }
    }
