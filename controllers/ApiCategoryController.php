<?php
    namespace App\Controllers;

    use \App\Core\DatabaseConnection;
    use \App\Models\CategoryModel;
    use \App\Models\AuctionModel;
    use \App\Core\ApiController;

    class ApiCategoryController extends ApiController {
        private $dbc;

        public function __construct(DatabaseConnection &$dbc) {
            $this->dbc = $dbc;
        }

        public function show($id) {
            $categoryModel = new CategoryModel($this->dbc);
            $category = $categoryModel->getById($id);

            if ($category === null) {
                $this->set('error', -10001);
                $this->set('message', 'Ne postoji trazena kategorija.');
                return;
            }

            $this->set('category', $category);

            $auctionModel = new AuctionModel($this->dbc);
            $auctions = $auctionModel->getAllActiveByCategoryId($id);
            $this->set('auctions', $auctions);
        }
    }
