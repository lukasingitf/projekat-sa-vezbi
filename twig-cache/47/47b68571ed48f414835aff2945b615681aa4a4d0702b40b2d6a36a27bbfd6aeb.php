<?php

/* Auction/show.html */
class __TwigTemplate_92a42a4a48bec6ae1c66d3dc67da4aecf4c4449c46250790e2a4cdf2a2f4b071 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("_global/index.html", "Auction/show.html", 1);
        $this->blocks = array(
            'sredina' => array($this, 'block_sredina'),
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_sredina($context, array $blocks = array())
    {
        // line 4
        echo "    <img src=\"/Nedelja01/assets/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "image_path", array()), "html", null, true);
        echo "\"
         alt=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "title", array()));
        echo "\">
    <h1>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "title", array()));
        echo "</h1>
    <p>";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "description", array()), "html", null, true);
        echo "</p>
    <p>Pocetak aukcije: ";
        // line 8
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "starts_at", array()), "j. n. Y.", "Europe/Belgrade"), "html", null, true);
        echo "</p>
    <p>Istek aukcije: ";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "ends_at", array()), "j. n. Y.", "Europe/Belgrade"), "html", null, true);
        echo "</p>
    <p>Trenutna cena: ";
        // line 10
        echo twig_escape_filter($this->env, ($context["latestPrice"] ?? null), "html", null, true);
        echo " EUR</p>
";
    }

    // line 13
    public function block_title($context, array $blocks = array())
    {
        echo "- Auction ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["auction"] ?? null), "title", array()));
    }

    public function getTemplateName()
    {
        return "Auction/show.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  61 => 10,  57 => 9,  53 => 8,  49 => 7,  45 => 6,  41 => 5,  36 => 4,  33 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Auction/show.html", "C:\\xampp\\htdocs\\Nedelja01\\views\\Auction\\show.html");
    }
}
