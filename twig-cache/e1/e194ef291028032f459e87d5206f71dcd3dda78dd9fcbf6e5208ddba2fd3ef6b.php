<?php

/* _global/index.html */
class __TwigTemplate_929a20cdb667906fd812fcff37c009896989a1e170fc8aedf91dc0cf0b4343a5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'sredina' => array($this, 'block_sredina'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>Moj sajt ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"/Nedelja01/assets/css/main.css\">
    </head>
    <body>
        <div class=\"holder\">
            <div class=\"sidebar\">
                Sidebar
            </div>

            <div class=\"banners\">
                <a href=\"/Nedelja01/\" class=\"banner\">
                    <img src=\"/Nedelja01/assets/images/site/banner.png\" alt=\"Banner 1\">
                </a>
            </div>

            <div class=\"social\">
                <a href=\"#\"><img src=\"/Nedelja01/assets/images/site/social/fb.png\" alt=\"FB\"></a>
                <a href=\"#\"><img src=\"/Nedelja01/assets/images/site/social/tw.png\" alt=\"TW\"></a>
                <a href=\"#\"><img src=\"/Nedelja01/assets/images/site/social/gp.png\" alt=\"G+\"></a>
                <a href=\"#\"><img src=\"/Nedelja01/assets/images/site/social/yt.png\" alt=\"YT\"></a>
                <a href=\"#\"><img src=\"/Nedelja01/assets/images/site/social/li.png\" alt=\"LI\"></a>
            </div>

            <div class=\"search-holder\">
                <form method=\"post\" action=\"/Nedelja01/search\">
                    <input type=\"text\" name=\"q\" placeholder=\"Kljucne reci...\">
                    <button type=\"submit\">
                        Pretraga
                    </button>
                </form>
            </div>

            <nav class=\"main-menu\">
                <ul>
                    <li><a href=\"/Nedelja01/\">Home</a>
                    <li><a href=\"#\">Item 2</a>
                    <li><a href=\"#\">Item 3</a>
                    <li><a href=\"#\">Item 4</a>
                    <li><a href=\"#\">Item sa<br>duzim imenom<br>ima vise redova</a>
                    <li><a href=\"#\">Item 6</a>
                    <li><a href=\"#\">Item 7</a>
                    <li><a href=\"#\">Item 8</a>
                    <li><a href=\"/Nedelja01/contact\">Contact</a>
                </ul>
            </nav>
            <main>
                ";
        // line 51
        $this->displayBlock('sredina', $context, $blocks);
        // line 53
        echo "            </main>
            <footer class=\"footer\">
                &copy; 2018 - Ta&amp;Ta
            </footer>
        </div>
    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "- Home page";
    }

    // line 51
    public function block_sredina($context, array $blocks = array())
    {
        // line 52
        echo "                ";
    }

    public function getTemplateName()
    {
        return "_global/index.html";
    }

    public function getDebugInfo()
    {
        return array (  102 => 52,  99 => 51,  93 => 5,  82 => 53,  80 => 51,  31 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_global/index.html", "C:\\xampp\\htdocs\\Nedelja01\\views\\_global\\index.html");
    }
}
