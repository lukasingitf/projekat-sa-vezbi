/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100121
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 100121
 File Encoding         : 65001

 Date: 07/05/2018 13:06:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auction
-- ----------------------------
DROP TABLE IF EXISTS `auction`;
CREATE TABLE `auction`  (
  `auction_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `starting_price` decimal(10, 2) UNSIGNED NOT NULL,
  `starts_at` datetime(0) NOT NULL,
  `ends_at` datetime(0) NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `category_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`auction_id`) USING BTREE,
  INDEX `fk_auction_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `fk_auction_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auction
-- ----------------------------
INSERT INTO `auction` VALUES (2, '2018-03-01 15:35:33', 'Lot rimskih novcica', 'images/2-lot.jpg', 'Ovo je lot rimskih novcica razlicitih stanja.', 1300.00, '2018-02-26 12:46:54', '2018-03-09 12:46:57', 1, 3);
INSERT INTO `auction` VALUES (3, '2018-03-05 12:47:53', 'Ulje na platnu - pejzaz', 'images/3-slika.jpg', 'Ovo je slika planinskog pejzaza u jesen - tehnbika: ulje na platnu.', 4599.00, '2018-03-09 12:47:45', '2018-03-14 12:47:48', 1, 2);
INSERT INTO `auction` VALUES (4, '2018-03-12 13:09:25', 'Lot grckih novcica', 'images/4-lot-novcici.jpg', 'Ovo je lot grckih novcica... ni jedan nije kompletan.', 340.00, '2017-03-11 11:34:09', '2017-03-20 11:34:09', 1, 3);
INSERT INTO `auction` VALUES (5, '2018-03-15 14:06:39', 'Muzička kutija sa figurom majmuna u persijskoj nošnji sa činelama', 'images/5-box-monkey.jpg', 'Ispravna muzička kutija sa figurom majmuna obučenog u persijsku nošnju sa činelama koji sedi na poklopcu kutije.', 665.00, '2017-03-11 11:34:09', '2017-03-21 12:34:09', 1, 4);
INSERT INTO `auction` VALUES (6, '2018-03-15 14:07:37', 'Slika sumskog pejzaza', 'images/6-pejzaz.jpg', 'Autor nije poznat. Pogledajte fotografiju slike.', 123.00, '2017-03-11 11:34:09', '2018-03-24 12:47:48', 1, 2);

-- ----------------------------
-- Table structure for auction_view
-- ----------------------------
DROP TABLE IF EXISTS `auction_view`;
CREATE TABLE `auction_view`  (
  `auction_view_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auction_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`auction_view_id`) USING BTREE,
  INDEX `fk_auction_view_auction_id`(`auction_id`) USING BTREE,
  INDEX `auction_view_ip_address_idx`(`ip_address`) USING BTREE,
  CONSTRAINT `fk_auction_view_auction_id` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auction_view
-- ----------------------------
INSERT INTO `auction_view` VALUES (1, '2018-04-14 14:17:15', 4, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (2, '2018-04-14 14:17:20', 4, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (3, '2018-04-14 14:17:20', 4, '10.2.3.100', NULL);
INSERT INTO `auction_view` VALUES (4, '2018-04-14 14:17:21', 4, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (5, '2018-04-14 14:17:34', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (7, '2018-04-14 14:20:56', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (8, '2018-04-14 14:22:12', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (9, '2018-04-14 14:22:13', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (10, '2018-04-14 14:22:13', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (11, '2018-04-16 13:04:36', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (12, '2018-04-16 13:28:12', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (13, '2018-04-16 13:28:14', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (14, '2018-04-16 13:28:14', 2, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (15, '2018-04-16 13:28:14', 2, '10.2.3.100', 1);
INSERT INTO `auction_view` VALUES (16, '2018-04-16 13:28:57', 3, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (17, '2018-04-16 13:28:58', 3, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (18, '2018-04-16 13:28:59', 3, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (19, '2018-04-16 13:32:30', 3, '192.100.10.8', NULL);
INSERT INTO `auction_view` VALUES (20, '2018-04-19 14:07:16', 2, '192.100.10.8', NULL);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  UNIQUE INDEX `uq_category_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 'Antikviteti');
INSERT INTO `category` VALUES (5, 'Nova kategorija');
INSERT INTO `category` VALUES (3, 'Numizmatika');
INSERT INTO `category` VALUES (4, 'Some other things');
INSERT INTO `category` VALUES (2, 'Umetnost');

-- ----------------------------
-- Table structure for offer
-- ----------------------------
DROP TABLE IF EXISTS `offer`;
CREATE TABLE `offer`  (
  `offer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) UNSIGNED NOT NULL,
  `auction_id` int(11) UNSIGNED NOT NULL,
  `price` decimal(10, 2) UNSIGNED NOT NULL,
  PRIMARY KEY (`offer_id`) USING BTREE,
  INDEX `fk_offer_user_id`(`user_id`) USING BTREE,
  INDEX `fk_offer_auction_id`(`auction_id`) USING BTREE,
  CONSTRAINT `fk_offer_auction_id` FOREIGN KEY (`auction_id`) REFERENCES `auction` (`auction_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_offer_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of offer
-- ----------------------------
INSERT INTO `offer` VALUES (1, '2018-03-01 15:39:49', 2, 2, 5000.00);
INSERT INTO `offer` VALUES (2, '2018-03-01 15:39:49', 3, 2, 5800.00);
INSERT INTO `offer` VALUES (3, '2018-03-15 14:08:43', 2, 5, 690.00);
INSERT INTO `offer` VALUES (4, '2018-03-15 14:08:48', 3, 5, 750.00);
INSERT INTO `offer` VALUES (5, '2018-03-15 15:26:43', 2, 6, 500.00);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `forename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `uq_user_username`(`username`) USING BTREE,
  UNIQUE INDEX `uq_user_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '2018-03-01 15:35:22', 'pera-peric', 'pperic@mejl.rs', 'HASH', 'Pera', 'Peric', 1);
INSERT INTO `user` VALUES (2, '2018-03-01 15:39:22', 'tijana-tijanic', 'ttijanic@mejl.rs', 'HASH', 'Tijana', 'Tijanic', 1);
INSERT INTO `user` VALUES (3, '2018-03-01 15:39:22', 'katarina-katic', 'kkatic@mejl.rs', 'HASH', 'Katarina', 'Katic', 1);

-- ----------------------------
-- Table structure for user_login
-- ----------------------------
DROP TABLE IF EXISTS `user_login`;
CREATE TABLE `user_login`  (
  `user_login_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_login_id`) USING BTREE,
  INDEX `fk_user_login_user_id`(`user_id`) USING BTREE,
  INDEX `user_login_ip_address_idx`(`ip_address`) USING BTREE,
  CONSTRAINT `fk_user_login_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
